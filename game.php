<?php

use Sogexia\Entity\Card;
use Sogexia\Game\Battle;
use Sogexia\GameRunner;
use Sogexia\Entity\Player;
use Sogexia\Entity\Deck;

require __DIR__ . '/config/bootstrap.php';

// on instancie le jeu auquel on veut jouer
$game = new Battle();

// création des cartes
$cards = array();
for ($i = 1; $i <= 52; $i++) {
    $cards[] = new Card($i);
}

// Création des joueurs
$players = array(
    new Player('Player 1'),
    new Player('Player 2'),
);

try {
    // création du jeu de cartes avec les cartes générées précedement
    $deck = new Deck($cards);

    // création d'une partie avec : le jeu, le jeu de cartes, les joueurs
    $gamePlayer = new GameRunner($game, $deck, $players);

    // c'est parti, on lance le jeu
    $gamePlayer->play();

    // on affiche le nom du gagnant
    $gamePlayer->displayWinner();
} catch (Exception $e) {
    // gestion des erreurs, si souci au cours de la partie
    echo sprintf("Error occurred - %s\n", $e->getMessage());
}

exit;
