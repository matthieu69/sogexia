# Game - Battle

Jeu de carte simulant une bataille

## Pré requis

Le jeu est compatible avec PHP 7.1

## Lancer le jeu

Pour lancer le jeux, il faut se placer a la racine du projet et executer la ligne de commande :
    
    php game.php
    
## Configuration

On peut configurer dans le fichier game.php:

* Le nombre de joueurs
* le nombre de cartes dans le jeu
* le jeu auquel jouer (pour le moment un seul jeu, la bataille)