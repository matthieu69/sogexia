<?php

namespace Sogexia\Game;

use Sogexia\Entity\Deck;
use Sogexia\Entity\PlayedCard;
use Sogexia\Entity\Player;
use Sogexia\GameInterface;

/**
 * Class Battle
 * @package Sogexia\Game
 */
class Battle implements GameInterface
{
    /**
     * @param Deck $deck
     * @param array $players
     * @return mixed|void
     */
    public function start(Deck $deck, array $players)
    {
        if (count($players) < 2) {
            throw new \RuntimeException('The Battle game require at least 2 players or more');
        }

        $deck = $this->shuffleCard($deck);
        $this->distribute($deck, $players);
    }

    /**
     * Resolve the winner when the players played one card
     *
     * @param PlayedCard[] $playedCards
     * @return PlayedCard
     */
    public function whoWon(array $playedCards): PlayedCard
    {
        if (count($playedCards) < 2) {
            throw new \RuntimeException('we need at least 2 cards to resolve who won');
        }

        $playedCardWinner = array_shift($playedCards);

        foreach ($playedCards as $playedCard) {
            if ($playedCard->getCard()->getValue() > $playedCardWinner->getCard()->getValue()) {
                $playedCardWinner = $playedCard;
            }
        }

        return $playedCardWinner;
    }

    /**
     * @param Player $player
     */
    public function rewardTheWinner(Player $player)
    {
        $player->addScore(1);
    }

    /**
     * @param Deck $deck
     * @return Deck
     */
    protected function shuffleCard(Deck $deck): Deck
    {
        $cards = $deck->getCards();
        shuffle($cards);
        $deck->setCards($cards);

        return $deck;
    }

    /**
     * Distribute cards between the players
     *
     * @param Deck $deck
     * @param Player[] $players
     */
    protected function distribute(Deck $deck, array $players)
    {
        $cards = $deck->getCards();
        $countCards = count($cards);
        $countPlayers = count($players);

        $nbCardsByPlayer = floor($countCards / $countPlayers);

        foreach ($players as $player) {
            $deckPlayer = new Deck(array_splice($cards, 0, $nbCardsByPlayer));
            $player->setDeck($deckPlayer);
        }
    }

    /**
     * @param Player[] $players
     * @return Player
     */
    public function resolveWinner(array $players): Player
    {
        $winner = array_shift($players);

        foreach ($players as $player) {
            if ($player->getScore() == $winner->getScore()) {
                throw new \RuntimeException(
                    sprintf(
                        "The players %s and %s have the same score '%s', retry a new game",
                        $player->getName(),
                        $winner->getName(),
                        $player->getScore()
                    )
                );
            }

            if ($player->getScore() > $winner->getScore()) {
                $winner = $player;
            }
        }

        return $winner;
    }

    /**
     * @param Deck $deck
     * @param Player[] $players
     * @return bool
     */
    public function isOver(Deck $deck, array $players): bool
    {
        return (!$players[0]->haveCard());
    }

    /**
     * @param Deck $deck
     * @param array $players
     * @return PlayedCard[]
     */
    public function play(Deck $deck, array $players): array
    {
        $playedCards = array();
        foreach ($players as $player) {
            $playedCards[] = new PlayedCard($player->play(), $player);
        }

        return $playedCards;
    }
}
