<?php

namespace Sogexia;

use Sogexia\Entity\Deck;
use Sogexia\Entity\PlayedCard;
use Sogexia\Entity\Player;

/**
 * Interface GameInterface
 * @package Sogexia
 */
interface GameInterface
{
    /**
     * @param Deck $deck
     * @param array $players
     * @return mixed
     */
    public function start(Deck $deck, array $players);

    /**
     * @param array $playedCards
     * @return PlayedCard
     */
    public function whoWon(array $playedCards): PlayedCard;

    /**
     * @param Player $player
     * @return mixed
     */
    public function rewardTheWinner(Player $player);

    /**
     * @param Player[] $players
     * @return Player
     */
    public function resolveWinner(array $players): Player;

    /**
     * @param Deck $deck
     * @param array $players
     * @return bool
     */
    public function isOver(Deck $deck, array $players): bool;

    /**
     * @param Deck $deck
     * @param array $players
     * @return array
     */
    public function play(Deck $deck, array $players): array;
}
