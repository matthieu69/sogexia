<?php

namespace Sogexia;

use Sogexia\Entity\Deck;
use Sogexia\Entity\PlayedCard;
use Sogexia\Entity\Player;

/**
 * Class GameRunner
 * @package Sogexia
 */
class GameRunner
{
    /**
     * @var GameInterface
     */
    protected $game;

    /**
     * @var Deck
     */
    protected $deck;

    /**
     * @var Player[]
     */
    protected $players;

    /**
     * GameRunner constructor.
     * @param GameInterface $game
     * @param Deck $deck
     * @param array $players
     */
    public function __construct(GameInterface $game, Deck $deck, array $players)
    {
        if ($deck->isEmpty()) {
            throw new \RuntimeException('The deck must contains cards');
        }
        $this->deck = $deck;
        $this->game = $game;
        $this->setPlayers($players);
    }

    /**
     * @param array $players
     * @return $this
     */
    public function setPlayers(array $players)
    {
        $this->players = array();
        foreach ($players as $player) {
            $this->addPlayer($player);
        }

        return $this;
    }

    /**
     * @param Player $player
     * @return $this
     */
    public function addPlayer(Player $player)
    {
        if (in_array($player, $this->players)) {
            throw new \RuntimeException('The player is already in the game');
        }
        $this->players[] = $player;
        return $this;
    }

    /**
     * Start to play at the game
     */
    public function play()
    {
        $this->game->start($this->deck, $this->players);

        while (!$this->game->isOver($this->deck, $this->players)) {
            $this->playCard();
        }
    }

    /**
     * Each player play a card
     */
    public function playCard()
    {
        $playedCards = $this->game->play($this->deck, $this->players);

        $playedCard = $this->game->whoWon($playedCards);

        $this->game->rewardTheWinner($playedCard->getPlayer());

        $this->displayWinnerPlayedCard($playedCard);
    }

    /**
     * @return Player
     */
    public function resolveWinner(): Player
    {
        return $this->game->resolveWinner($this->players);
    }

    /**
     * Show the name of the winner
     */
    public function displayWinner()
    {
        $player = $this->resolveWinner();
        echo sprintf("The winner is %s\n", $player->getName());
    }

    /**
     * @param PlayedCard $playedCard
     */
    protected function displayWinnerPlayedCard(PlayedCard $playedCard)
    {
        echo sprintf(
            "%s win with the card '%s'\n",
            $playedCard->getPlayer()->getName(),
            $playedCard->getCard()->getValue()
        );
    }
}
