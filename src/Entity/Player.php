<?php

namespace Sogexia\Entity;

/**
 * Class Player
 * @package Sogexia\Entity
 */
class Player
{
    /**
     * @var Deck
     */
    protected $deck;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $score;

    /**
     * Player constructor.
     * @param string $name
     * @param Deck|null $deck
     */
    public function __construct(string $name, Deck $deck = null)
    {
        $this->name = $name;
        if (!is_null($deck)) {
            $this->setDeck($deck);
        }
        $this->score = 0;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Deck $deck
     * @return $this
     */
    public function setDeck(Deck $deck)
    {
        $this->deck = $deck;
        return $this;
    }

    /**
     * @return Card
     */
    public function play(): Card
    {
        return $this->deck->getNextCard();
    }

    /**
     * @return bool
     */
    public function haveCard(): bool
    {
        return (!$this->deck->isEmpty());
    }

    /**
     * @param int $score
     * @return int
     */
    public function addScore(int $score): int
    {
        $this->score+= $score;
        return $this->score;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }
}
