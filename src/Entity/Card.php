<?php

namespace Sogexia\Entity;

/**
 * Class Card
 * @package Sogexia\Entity
 */
class Card
{

    /**
     * @var int
     */
    protected $value;

    /**
     * Card constructor.
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
