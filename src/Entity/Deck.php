<?php

namespace Sogexia\Entity;

/**
 * Class Deck
 * @package Sogexia\Entity
 */
class Deck
{

    /**
     * @var Card[]
     */
    protected $cards;

    /**
     * Deck constructor.
     * @param array|null $cards
     */
    public function __construct(array $cards = null)
    {
        if (!is_null($cards)) {
            $this->setCards($cards);
        }
    }

    /**
     * @param array $cards
     * @return $this
     */
    public function setCards(array $cards)
    {
        $this->cards = array();
        foreach ($cards as $card) {
            $this->addCard($card);
        }
        return $this;
    }

    /**
     * @param Card $card
     * @return $this
     */
    public function addCard(Card $card)
    {
        if (in_array($card, $this->cards)) {
            throw new \RuntimeException(sprintf('The card "%s" is already in the deck', $card->getValue()));
        }
        $this->cards[] = $card;
        return $this;
    }

    /**
     * @return Card[]
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    /**
     * @return Card
     */
    public function getNextCard(): Card
    {
        return array_shift($this->cards);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->cards);
    }
}
