<?php

namespace Sogexia\Entity;

/**
 * Class PlayedCard
 * @package Sogexia\Entity
 */
class PlayedCard
{
    /**
     * @var Card
     */
    protected $card;

    /**
     * @var Player
     */
    protected $player;

    /**
     * PlayedCard constructor.
     * @param Card $card
     * @param Player $player
     */
    public function __construct(Card $card, Player $player)
    {
        $this->card = $card;
        $this->player = $player;
    }

    /**
     * @return Card
     */
    public function getCard(): Card
    {
        return $this->card;
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }
}
