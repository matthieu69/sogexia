<?php

namespace Test\Game;

use Sogexia\Entity\Deck;
use Sogexia\Entity\Player;
use Sogexia\Game\Battle;

class BattleTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @expectedException \RuntimeException
     */
    public function testExceptionStartMissingPlayer()
    {
        $battle = new Battle();

        $players = array(new Player('Player 1'));

        $battle->start(new Deck(), $players);
    }

    public function testRewardTheWinner()
    {
        $player = new Player('Player 1');

        $battle = new Battle();

        $battle->rewardTheWinner($player);

        $this->assertEquals(1, $player->getScore());

    }
}