<?php

namespace Test\Entity;

use Sogexia\Entity\Player;

class PlayerTest extends \PHPUnit\Framework\TestCase
{
    public function testAddScore()
    {
        $player = new Player('Player 1');

        $score = $player->addScore('3');

        $this->assertEquals(3, $score);

        $score = $player->addScore('2');

        $this->assertEquals(5, $score);
    }
}