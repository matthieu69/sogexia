<?php

namespace Test;

use Sogexia\Entity\Card;
use Sogexia\Entity\Deck;
use Sogexia\Entity\Player;
use Sogexia\GameInterface;
use Sogexia\GameRunner;

class GameRunnerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var GameRunner
     */
    protected $gameRunner;


    public function setUp()
    {
        $game = $this->createMock(GameInterface::class);

        $cards = [
            new Card(1),
            new Card(2),
            new Card(3),
            new Card(4),
        ];

        $deck = new Deck($cards);
        $players = [
            new Player('Player 1'),
            new Player('Player 2'),
        ];

        $this->gameRunner = new GameRunner($game, $deck, $players);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testExceptionAddPlayer()
    {
        $this->gameRunner->addPlayer(new Player('Player 1'));
    }
}